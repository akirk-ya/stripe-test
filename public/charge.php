<?php
/**
 * Created by PhpStorm.
 * User: Alison
 * Date: 7/11/18
 * Time: 2:15 PM
 */

 require_once('config.php');

  $token  = $_POST['stripeToken'];
  $email  = $_POST['stripeEmail'];


  $customer = \Stripe\Customer::create(array(
      'email' => $email,
      'source'  => $token
  ));

  $charge = \Stripe\Charge::create(array(
      'customer' => $customer->id,
      'amount'   => 500,
      'currency' => 'usd'
  ));

 $transactionId = $charge->id;

 $transactionDetails = \Stripe\Charge::retrieve($transactionId);

  echo '<h1>Successfully charged $.50!</h1>' . $transactionDetails->status;
?>

