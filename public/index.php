<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/app.css">
    <title>Stripe Test</title>
</head>
<body>
<div class="container">
    <?php require_once('./config.php'); ?>

    <form action="charge.php" method="post">
        <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="<?php echo $stripe['publishable_key']; ?>"
                data-description="testing"
                data-amount="500"
                data-locale="auto"></script>
    </form>

    <p>The status of the payment is either succeeded, pending, or failed.</p>

</div>


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>

</body>
</html>

